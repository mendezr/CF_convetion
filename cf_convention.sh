#!/bin/sh

# Para correr este programa:
#sh cf_convention.sh archivo.nc
#sh cf_convention.sh *.nc    (acepta wildcards)


# Carga nco de chaman
#module load intel/cdf-bundle2018    

for file in "$@"; do
    printf 'Cambiando el archivo "%s"\n' "$file"
    # Cambia nombre de variables
    ncrename -d ZAX2,DEPTH -v ZAX2,DEPTH $file
    ncrename -d TFEC,TIME -v TFEC,TIME $file
    ncrename -d YAXIS,Y  $file
    ncrename -d XAXIS,X $file

    # Borra atributo no deseado en todas las variables 
    ncatted -O -a bounds,,d,, $file
    ncatted -O -a long_name_mod,,d,, $file
    # Borra variable y lo guarda en un nuevo netcdf
    ncks -O -x -v ZAX2_bnds $file out.nc
    
    
    # Esta parte es la que cambia los atributos de acuerdo a la CF-1.8

    # Agrega atributos nuevo
    # ncatted -O -a nombre_atributo,variable,c,c,"texto" out.nc
    ncatted -O -a standard_name,U,c,c,"sea_water_x_velocity" out.nc
    ncatted -O -a standard_name,V,c,c,"sea_water_y_velocity" out.nc
    ncatted -O -a standard_name,W,c,c,"upward_sea_water_velocity" out.nc
    ncatted -O -a standard_name,SALT,c,c,"sea_water_salinity" out.nc
    ncatted -O -a standard_name,TEMP,c,c,"sea_water_potential_temperature" out.nc
    ncatted -O -a standard_name,U_BAR,c,c,"barotropic_sea_water_x_velocity" out.nc
    ncatted -O -a standard_name,V_BAR,c,c,"barotropic_sea_water_y_velocity" out.nc
    ncatted -O -a standard_name,ZETA,c,c,"sea_surface_height_above_mean_sea_level" out.nc
    ncatted -O -a standard_name,MIXED_LAYER,c,c,"ocean_mixed_layer_thickness" out.nc
    ncatted -O -a standard_name,LON,c,c,"longitude" out.nc
    ncatted -O -a standard_name,LAT,c,c,"latitude" out.nc
    ncatted -O -a units,U,c,c,"m s-1" out.nc
    ncatted -O -a units,V,c,c,"m s-1" out.nc
    ncatted -O -a units,W,c,c,"m s-1" out.nc
    ncatted -O -a units,TEMP,c,c,"degree_C" out.nc
    ncatted -O -a units,U_BAR,c,c,"m s-1" out.nc
    ncatted -O -a units,V_BAR,c,c,"m s-1" out.nc
    ncatted -O -a units,ZETA,c,c,"m" out.nc
    ncatted -O -a units,LON,c,c,"degree_east" out.nc
    ncatted -O -a units,LAT,c,c,"degree_north" out.nc
    # Modifica atributo ya existente ("o" - overwrite)
    ncatted -O -a units,DEPTH,o,c,"m" out.nc 
    ncatted -O -a units,MIXED_LAYER,o,c,"m" out.nc


    # Regresa al nombre original del archivo
    mv out.nc $file 

  done
