#!/share/apps/anaconda-python-3.5/bin/python
# coding: utf-8

"""
Script para aplicar la convencion CF-1.6 a los archivos de la salidas
del modelo ROMS para el pronostico del Golfo de Mexico.

El script genera un archivo netCDF con el mismo nombre con la
convecion CF-1.6 para las variables especificas en la variable vars

El unico modulo que no vienen por default es xarray.

Modo de uso:

Para correrlo sobre un solo archivo
- python CF_conventions_roms.py file.nc

Para correrlo con varios archivos
- python CF_conventions_roms.py *.nc

Si se va a usar como programa agregar la siguiente linea al .bashrc
export PATH=$PATH:/LUSTRE/mendezr/programas/

- CF_conventions_roms file.nc
- CF_conventions_roms *.nc

"""
import sys
import logging
from datetime import datetime
import xarray as xr

# Logging
format = '%(asctime)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=format)

# Lee los archivos de los argumentos
files = sys.argv[1:]


def standard_name(ds):
    """
    Funcion que le agrega los standar names a las variables
    """
    std_name = {'U': {'standard_name': 'sea_water_x_velocity'},
                'V': {'standard_name': 'sea_water_y_velocity'},
                'W': {'standard_name': 'upward_sea_water_velocity'},
                'SALT': {'standard_name': 'sea_water_salinity'},
                'TEMP': {'standard_name': 'sea_water_potential_temperature'},
                'UBAR': {'standard_name': 'barotropic_sea_water_x_velocity'},
                'VBAR': {'standard_name': 'barotropic_sea_water_y_velocity'},
                'ZETA': {'standard_name': 'sea_surface_height_above_mean_sea_level'},
                'MIXED_LAYER': {'standard_name': 'ocean_mixed_layer_thickness'},
                }

    for var in std_name.keys():
        ds[var].attrs['standard_name'] = std_name[var]['standard_name']

    # Estos los tuve que poner a mano
    ds['LON'].attrs['standard_name'] = 'longitude'
    ds['LON'].attrs['units'] = 'degree_east'
    ds['LAT'].attrs['standard_name'] = 'latitude'
    ds['LAT'].attrs['units'] = 'degree_east'
    ds['DEPTH'].attrs['standard_name'] = 'depth'
    ds['DEPTH'].attrs['positive'] = 'down'
    ds['DEPTH'].attrs['units'] = 'm'

    # Este tiempo lo saque del ROMS
    ds['TIME'].encoding['units'] = 'days since 1901-01-15 00:00:00'


def copy_attrs(ds, ds_template):
    variables_on_template = ['U', 'V', 'ZETA', 'SALT', 'TEMP', 'UBAR',
                             'VBAR']
    for var in variables_on_template:
        ds[var].attrs['long_name'] = ds_template[var.lower()].attrs['long_name']
        ds[var].attrs['units'] = ds_template[var.lower()].attrs['units']


def delete_attrs(ds):
    for k in ds.keys():
        try:
            logging.debug('Borrando _FillValue')
            del ds[k].attrs['_FillValue']
        except KeyError:
            logging.debug('No se encontro _FillValue')
            pass

def new_dataframe(fname):
    """
    Funcion que crea un nuevo Dataset, esta Dataset es diferente
    al original ya que usa una sola x, y en lugar de las que dos
    que vienen en el archivo original, otro cambio es que le cam-
    bia el nombre a ZAX2 por DEPTH.

    Por ultimo les agrega los atributos del archivo original y
    borra el atributo history de cada variable.

    El script busca la variable ZAX2, si no la encuentra termina
    la funcion.
    """

    # Abre el archivo original
    n = xr.open_dataset(fname)

    if 'ZAX2' not in n:
        return

    # Variables a crear en el nuevo archivo netCDF
    vars = {'ZETA': (['TIME', 'Y', 'X'], n['ZETA']),
            'TEMP': (['TIME', 'DEPTH', 'Y', 'X'], n['TEMP']),
            'SALT': (['TIME', 'DEPTH', 'Y', 'X'], n['SALT']),
            'U': (['TIME', 'DEPTH', 'Y', 'X'], n['U']),
            'V': (['TIME', 'DEPTH', 'Y', 'X'], n['V']),
            'W': (['TIME', 'DEPTH', 'Y', 'X'], n['W']),
            'MIXED_LAYER': (['TIME', 'Y', 'X'], n['MIXED_LAYER']),
            'UBAR': (['TIME', 'Y', 'X'], n['U_BAR']),
            'VBAR': (['TIME', 'Y', 'X'], n['V_BAR'])}

    # Genera un nuevo Dataset
    ds = xr.Dataset(vars,
                    coords={'DEPTH': (['DEPTH'], n.ZAX2),
                            'LON': (['Y', 'X'], n.LON),
                            'LAT': (['Y', 'X'], n.LAT),
                            'TIME': (['TIME'], n.TFEC)})

    """
    Agrega los atributos para todas las variables dentro del archivo
    original, los try y except son para evitar que el programa se cierre
    si no encuentra el atributo history en la variable.
    """
    # ds_template = xr.open_dataset('/LUSTRE/mendezr/programas/roms_template.nc')
    ds_template = xr.open_dataset('./roms_template.nc')
    copy_attrs(ds, ds_template)
    standard_name(ds)
    delete_attrs(ds)
    ds['MIXED_LAYER'].attrs['long_name'] = n['MIXED_LAYER'].attrs['long_name']

    # Agrega los atributos del archivo netCDF
    atts = {'Convetions': 'CF-1.6',
            'production': 'ROMS',
            'TimeStamp': datetime.strftime(datetime.now(),
                                           '%Y-%b-%d %H:%m:%S GMT-0800'),
            'file_name': fname}
    ds.attrs = atts

    # Agrega el history del archivo original
    ds.attrs['history'] = n.attrs['history']

    return ds


for file in files:
    ds = new_dataframe(file)
    if ds:
        logging.info(f'Convertiendo el archivo:')
        logging.info(f'{file}')
        ds.to_netcdf(f'{file}', unlimited_dims=['TIME'])
